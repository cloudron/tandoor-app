[0.1.0]
* Initial version for Tandoor with 1.4.9
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.4.9)

[0.2.0]
* Fix media upload

[0.3.0]
* Update Tandoor to 1.4.10
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.4.10)
* fixed release notifications (thanks to @gabe565 #2437)
* updated django and fixed file upload (thanks to ambroisie #2458)
* updated translations

[0.4.0]
* Update Tandoor to 1.4.11
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.4.11)
* added noindex to header to prevent indexing content by search engines (thanks to screendriver #2435)
* added ability to set gunicorn logging parameter to .env (thanks to gloriousDan #2470)
* improved plugin functionality

[1.0.0]
* First stable package release
* Update Tandoor to 1.4.12
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.4.12)
* added allow plugins to define dropdown nav entries
* fixed json importer not working since its missing source_url attribute
* updated open data plugin

[1.1.0]
* Update Tandoor to 1.5.0
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.0)
* added unit conversion
* tandoor can now automatically convert your ingredients to different units
* conversion between all common metric and imperial units works automatically (within either weight or volume)
* conversions to convert between weight and volume for certain foods or to convert between special units (like pcs or your favourite cup) can be added manually
* currently conversions are used for property calculation, in the future many more features are possible with this
* added food properties
* every food can have different properties like nutrition, price, allergens or whatever you like to track
* these properties are then automatically calculated for every individual recipe
* the URL importer now automatically imports nutrition information properties from websites if possible
* in the future this can be integrated into different modules like shopping (price) or meal plans (nutrition)
* added open data importer
* The Tandoor Open Data Project aims to provide a community curated list of basic data for your tandoor instance
* Feel free to participate in its growth to help everyone improve their Tandoor workflows
* improved food editor (much cleaner, supports new features)
* added admin options to delete unused steps and ingredients (thanks to @smilerz #2488)
* added Norwegian to available languages #2487
* fixed hide bottom navigation in print view (thanks to jwr1 #2478)
* fixed url import edge case with arrays as names #2429
* fixed HowToSteps in Nextcloud Cookbook imports #2428
* fixed edge cases with the rezeptsuite importer #2467
* fixed recipesage servings and time import
* improved lots of functionality regarding plugins (still otherwise undocumented as very early stages)

[1.1.1]
* Update Tandoor to 1.5.1
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.1)
* fixed meal plan view
* fixed error with keyword automation

[1.1.2]
* Update Tandoor to 1.5.2
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.2)
* fixed merging foods would delete all food properties #2513
* fixed uniqueness check failing with open data slug models #2512

[1.1.3]
* Update Tandoor to 1.5.3
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.3)
* improved don't show the properties view if no property types are present in space
* improved automatically adding schema specific attributes if not present in json importer #2426
* fixed issue with not begin able to add decimal amounts in property values #2518
* fixed issue when creating food and adding properties at the same time
* fixed text color in nav to light for some background colors
* fixed broken images could fail the tandoor importer
* added TrueNAS Portainer installation instructions to the docs (thanks to 16cdlogan #2517)

[1.1.4]
* Update Tandoor to 1.5.4
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.4)
* added default valid_until date for invite links
* added ability to add a note to invite links which is transferred to the space permission for later recognition
* added experimental dark theme (does not work yet on many pages)
* improved query performance of shopping list (should be significantly faster on large lists)
* improved show optional fields in generic forms
* improved source_url is now included in recipe exports (thanks michael-genson to #2531)
* improved added filtering to some commonly used administrative admin pages
* fixed navbar color for non logged in users
* fixed issue when importing lists of ingredients with empty rows #2519
* fixed an issue with the food editor always showing "g" as the properties unit #2518
* fixed error message when no plugins are installed
* updated base alpine version to 3.18 to fix raspi builds (thanks to @gloriousDan #2522)
* updated docs to add some notes to default docker compose files and update pg version (thanks @gloriousDan to #2521)

[1.1.5]
* Update Tandoor to 1.5.5
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.5)
* added ability to set base unit for units to improve conversion ability #2537
* added ability to set URL's for foods to link to external websites
* added back description field to food editor (thanks to titilambert #2541)
* improved system information page showing commit ref even in non docker setups #156
* improved multi url import to prevent overloading the backend/external service #2542
* changed guest users can no longer copy recipes #2547
* fixed copied ingredients being linked to each other #2532
* fixed issue with rating sort order (thanks to @smilerz #2563)
* fixed embedded PDF viewer not respecting sub path setups #2424
* updated django and several other dependencies

[1.1.5-1]
* Always run database migration

[1.1.6]
* Update Tandoor to 1.5.6
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.6)
* added auto meal planning (thanks to AquaticLava #2468)
* added two new automations never unit and tranpose words see docs (thanks to @smilerz #2432)
* added option to hide step ingredients (thanks to srwareham #2539)
* added ability to share a link to tandoor to import it #2573
* improved documentation of the reverse proxy auth feature to prevent misconfiguration (thanks to boppy)
* improved url import image fetching reliability #2466
* fixed merge, move and automate not working for food models #2584
* fixed wrong per serving calculation of recipe properties #2579
* fixed typo in beta warning message (thanks to WoosterInitiative #2592)
* updated lots of dependencies
* updated translations (thanks to the awesome translation community over on https://translate.tandoor.dev)

[1.2.0]
* Rename `/app/data/.env` to `/app/data/env`

[1.3.0]
* Update base image to 4.2.0

[1.3.1]
* Update Tandoor to 1.5.9
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.9)

[1.3.2]
* Update Tandoor to 1.5.10
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.10)
* added property editor to quickly edit properties for all foods on a given recipe
* improved error messages when importing recipes / other places (thanks to jrester #2728)
* improved adding to the meal plan from the recipe context menu now has a default end date
* improved lots of documentation about the installation/update process (thanks to @smilerz #2758)
* improved made to_date in meal plan api optional
* changed renamed TIMEZONE environment variable to TZ, old is deprecated but will likely stay for a long time (thanks to @smilerz #2758)
* changed no longer show warning on property view when individual values are 0 as that might be on purpose
* changed automatically open the ingredient editor in a new tab
* fixed meal plan items breaking when spanning multiple weeks/periods #2678
* fixed error when URL importing recipes with to long descriptions (thanks to @smilerz #2763)
* fixed issue with the youtube importer
* fixed database connection strings with port numbers (thanks to tourn #2771)
* fixed copying recipes with properties would link properties together #2720
* fixed description overlays breaking obscured by time information #2743 (thanks flomero for helping)
* updated some dependencies

[1.3.3]
* Update Tandoor to 1.5.11
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.11)
* added new theming/styling options
  * upload custom images for favicon and PWA app icon
  * upload a custom logo for your space
  * space settings can override user settings for theming
  * spaces can upload custom CSS overrides
  * allow users to disable showing the tandoor/space logo
  * allow changing navigation background color to any color desired
  * allow switching navigation text color between dark/light (different effects depending on theme)
* added postgres deprecation warning on system page (thanks to @smilerz #2730)
* added migration overview to system page
* added several new admin functions (thanks to @smilerz #2823)
* added documentation page for all configuration parameters, removed non required parameters from .env.template

[1.3.4]
* Update Tandoor to 1.5.12
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.12)
* added rating to recipe card (thanks to @smilerz #2822)
* added ability to do math and scale an arbitrary number with the templating syntax (thanks to RomRider #2380)
* added apache subfolder example to docs (thanks to adjokic #2887)
* added PWA install docs for firefox on android (thanks to axeleroy #2785)
* improved clickable area for ingredient notes (thanks to harry48225 #2859)
* improved make gunicorn and nginx listen to ipv6 (thanks to swnf #2722)
* improved hide ingredient edit modal in import after clicking ok/delete #2882
* improved docs for manual docker nginx install (thanks to sohmc #2826)
* fixed content shift on meal plan mobile view obscuring bottom nav

[1.4.0]
* Update Tandoor to 1.5.14
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.14)
* New and improved UI
* significantly improved offline/low network capabilities
* Undo functionality
* Configure what kind of information to show directly in the list
* improved API performance
* allow merging of supermarket categories
* edit entries after they have been created

[1.4.1]
* Update Tandoor to 1.5.15
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.15)
* added the `default_page` setting back to the settings
* added shopping as an option for default page
* changed the PWA's default link now respects the default page setting (default is search so nothing changes for most users)
* fixed previously delayed entries might be missing from shopping list and auto-onhand not working (thanks to @smilerz #3048)
* fixed boot.sh would not properly work on systems with IPv6 disabled
* fixed system page breaking with certain PG versions (thanks ot @richid #3027)
* fixed error by deleting the currently selected supermarket
* fixed shopping category headers would stay even if they only contained postponed entries
* reverted meal plan ical API change to have optional parameters (will be rewritten in future updates)

[1.4.2]
* Update Tandoor to 1.5.16
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.16)
* improved test running speed, added test specific settings and cleaned up test system (thanks to @smilerz #3052)
* improved container startup speed for docker users by using pre-compiled static files (thanks to tooboredtocode #3055)
* fixed broken ability to change shopping categories of items that already had a category
* fixed system page error (thanks to @richid #3059)
* fixed error when calculating property leading to recipe not showing
* fixed home assistant sync failing in some conditions #3066, removed dependency to custom library (thanks to @Mikhail5555 #3067)
* updated several dependencies

[1.4.3]
* Update Tandoor to 1.5.17
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.17)
* reverted #3055 because it likely lead to errors on some setups #3099
* downgraded recipe-scrapers because scraper used in tests was broken

[1.4.4]
* Update Tandoor to 1.5.18
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.18)

[1.4.5]
* Update Tandoor to 1.5.19
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.19)
* improved recipe scraping functions (thanks to @smilerz #3227)
* improved logging and log settings (thanks to @mikhail5555 #3215)
* improved added us_cup as base unit (thanks to yuekaim #3260)
* fixed guest user errors on search (due to missing permission for keyword/food/space), now read only permission
* fixed /openapi/ endpoint not working due to missing package #3209
* fixed issue with importing urls #3249
* fixed README link to contribution guidelines (thanks to jknndy #3226)
* updated translations (thanks to all the awesome people helping with them on translate.tandoor.dev)

[1.4.6]
* Update Tandoor to 1.5.20
* [Full changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.20)
* fixes source import not working without a given url (thanks to @smilerz #3268)
* fixed typo in free space message (thanks to @Priyanshu1035 #3278)
* fixed Homeassistant connector working only in DEBUG mode (thansk to @mikhail5555 #3302)
* fixed Homeassistant connector not working with home assistant TODO entries without a description field (thanks to @ehowe #3305)
* updated several dependencies
* updated manual installation docs (thanks to IkkiOcean #3327)

[1.4.7]
* Update recipes to 1.5.21
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.21)
* **added** gourmet importer (thanks to [vitoller](https://github.com/vitoller) [#&#8203;3280](https://github.com/TandoorRecipes/recipes/issues/3280))
* **improved** paprika importer image handling (thanks to [@&#8203;tlambertz](https://github.com/tlambertz) [#&#8203;3361](https://github.com/TandoorRecipes/recipes/issues/3361))
* **fixed** mealie import failing when yield is null (thanks to [Chronophylos](https://github.com/Chronophylos) [#&#8203;3359](https://github.com/TandoorRecipes/recipes/issues/3359))
* **updated** some dependencies

[1.4.8]
* Update recipes to 1.5.22
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.22)
* **fixed** system view [#&#8203;3399](https://github.com/TandoorRecipes/recipes/issues/3399)

[1.4.9]
* Update recipes to 1.5.23
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.23)
* **fixed** system page (this time for real ...)
* **fixed** YouTube importer by switching library from pytube to pytubefix

[1.4.10]
* Update recipes to 1.5.24
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.24)
* **added** ability to set more secrets in files (thanks to [bishtawi](https://github.com/bishtawi) [#&#8203;3411](https://github.com/TandoorRecipes/recipes/issues/3411))
* **improved** secure rendering of instruction templates

[1.4.11]
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.4.12]
* Update recipes to 1.5.25
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.25)
* **added** warning if a recipe url is imported twice (thanks to [lavanyamehn](https://github.com/lavanyamehn) [#&#8203;3412](https://github.com/TandoorRecipes/recipes/issues/3412))
* **added** a health check to the default Dockerfile for external applications to monitor tandoors state (thanks to [zodac](https://github.com/zodac) [#&#8203;3416](https://github.com/TandoorRecipes/recipes/issues/3416))
* **fixed** error when using social providers to sign up
* **improved** system page (thanks to [@&#8203;igorsantos07](https://github.com/igorsantos07) [#&#8203;3448](https://github.com/TandoorRecipes/recipes/issues/3448))
* **updated** dependencies
* **improved** docs (thanks to [hawthorc](https://github.com/hawthorc) [#&#8203;3428](https://github.com/TandoorRecipes/recipes/issues/3428))

[1.4.13]
* Update recipes to 1.5.26
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.26)
* **updated** cryptography lib so build doesn't fail

[1.4.14]
* Update recipes to 1.5.27
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.27)
* **removed** healthcheck as it was causing issues for some people

[1.4.15]
* Update recipes to 1.5.29
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.29)
* **improved** paprika image import (thanks to [mitcdh](https://github.com/mitcdh) [#&#8203;3497](https://github.com/TandoorRecipes/recipes/issues/3497))
* **updated** several dependencies
* **changed** allow configuring external recipe sources only as superuser of the instance
* **changed** blacklist to prevent certain paths from being used in local external recipes (etc, root, medafiles, usr). If you are using any of them you need to change your configuration
* **changed** local external import only considers pdf and image types
* **changed** default nginx config to download mediafiles if opened (setting content disposition Attachement)
* **changed** allow only image, pdf and office files to be uploaded to the UserFile system
* **updated** django (security update)

[1.4.16]
* Update recipes to 1.5.30
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.30)
* **improved** recognition of notes as part of ingredient strings (thanks to [@&#8203;smilerz](https://github.com/smilerz) [`157af15`](https://github.com/TandoorRecipes/recipes/commit/157af15a2ad8477ca1da4c3156b9bce98f122122))
* **fixed** errors when deleting recipe images
* **fixed** error when importing a recipe

[1.4.17]
* Update recipes to 1.5.31
* [Full Changelog](https://github.com/TandoorRecipes/recipes/releases/tag/1.5.31)
* **fixed** image upload failing with upper case image extensions [#&#8203;3511](https://github.com/TandoorRecipes/recipes/issues/3511)

