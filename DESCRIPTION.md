## Overview

Manage your ever growing recipe collection online. Drop your collection of links and notes.

Get Tandoor and never look back onto a time without recipe management, storage, sharing and collaborative cooking!

This application is meant for people with a collection of recipes they want to share with family and friends or simply store them in a nicely organized way. A basic permission system exists but this application is not meant to be run as a public page.

## Core Features

* Manage your recipes - Manage your ever growing recipe collection
* Plan - multiple meals for each day
* Shopping lists - via the meal plan or straight from recipes
* Cookbooks - collect recipes into books
* Share and collaborate on recipes with friends and family

## All the must haves

* Optimized for use on mobile devices
* localized in many languages thanks to the awesome community
* Import your collection from many other recipe managers
* Many more like recipe scaling, image compression, printing views and supermarkets

