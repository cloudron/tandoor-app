#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const recipe = 'Nuddles';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password, alreadyAuthenticated = true) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/accounts/login/`);

        await waitForElement(By.xpath('//button[contains(text(), "Sign in using")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "Sign in using")]')).click();
        await waitForElement(By.xpath('//button[contains(text(), "Continue")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "Continue")]')).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.xpath('//input[@name="username"]'));
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();

            await browser.sleep(3000);
            await waitForElement(By.xpath('//div[contains(text(), "Successfully signed in")]'));
        } else {
            await browser.sleep(3000);
        }
    }

    async function createSpace() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//input[@value="Create Space"]'));
        await browser.findElement(By.xpath('//input[@value="Create Space"]')).click();
        browser.sleep(3000);

        await browser.findElements(By.xpath('//div[contains(@class, "alert-success") and contains(., "You have successfully created your own recipe space.")]'));
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn + '/');
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath('//a[contains(@href, "shopping")]')), TEST_TIMEOUT);
    }

    async function createRecipe(recipename) {
        await browser.get(`https://${app.fqdn}/new/recipe/`);
        await browser.sleep(5000);

        await browser.findElement(By.id('id_name')).sendKeys(recipename);
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//button[@type="submit" and contains(@class, "btn-success") and contains(., "Save")]')).click();
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//button[@type="button" and contains(@class, "btn-info") and contains(., "Save")]')).click();
    }

    async function checkRecipe(recipename) {
        await browser.get('https://' + app.fqdn + '/search');
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath('//div[contains(@class, "card-body")]//a[contains(@class, "text-body") and contains(text(), "' + recipename + '")]')), TEST_TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password, false));
    it('can create space', createSpace);
    it('can create Recipe', createRecipe.bind(null, recipe));
    it('check Recipe', checkRecipe.bind(null, recipe));

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can get the main page', getMainPage);
    it('check Recipe', checkRecipe.bind(null, recipe));

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can get the main page', getMainPage);
    it('check Recipe', checkRecipe.bind(null, recipe));

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password, true));
    it('can get the main page', getMainPage);
    it('check Recipe', checkRecipe.bind(null, recipe));

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id dev.tandoor.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password, true));
    it('can create space', createSpace);
    it('can get the main page', getMainPage);
    it('can create Recipe', createRecipe.bind(null, recipe));
    it('check Recipe', checkRecipe.bind(null, recipe));
    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('check Recipe', checkRecipe.bind(null, recipe));

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

