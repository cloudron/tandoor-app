#!/bin/bash

set -eu

echo "=> Creating directories"
mkdir -p /run/tandoor /run/nginx /app/data/data/{staticfiles,mediafiles}

echo "=> Get secret key"
if [[ ! -f /app/data/.secret_key ]]; then
    openssl rand -base64 42 > /app/data/.secret_key
fi
export SECRET_KEY=$(</app/data/.secret_key)

# migration
[[ -f /app/data/.env ]] && mv /app/data/.env /app/data/env

if [[ ! -f /app/data/env ]]; then
    echo "==> Copying default environment variables"
    cp /app/pkg/env.template /app/data/env
fi

echo "=> Configuring Tandoor"
cat > /run/tandoor/env << EOF
DEBUG=0
SQL_DEBUG=0
DEBUG_TOOLBAR=0

ALLOWED_HOSTS="${CLOUDRON_APP_DOMAIN}"

SECRET_KEY=${SECRET_KEY}

DB_ENGINE=django.db.backends.postgresql
POSTGRES_HOST=${CLOUDRON_POSTGRESQL_HOST}
POSTGRES_PORT=${CLOUDRON_POSTGRESQL_PORT}
POSTGRES_USER=${CLOUDRON_POSTGRESQL_USERNAME}
POSTGRES_PASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}
POSTGRES_DB=${CLOUDRON_POSTGRESQL_DATABASE}

STATIC_URL=/static/
MEDIA_URL=/media/

EMAIL_HOST=${CLOUDRON_MAIL_SMTP_SERVER}
EMAIL_PORT=${CLOUDRON_MAIL_SMTPS_PORT}
EMAIL_HOST_USER=${CLOUDRON_MAIL_SMTP_USERNAME}
EMAIL_HOST_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}
EMAIL_USE_TLS=0
EMAIL_USE_SSL=1
DEFAULT_FROM_EMAIL=${CLOUDRON_MAIL_FROM}

SOCIAL_PROVIDERS="allauth.socialaccount.providers.openid_connect"
SOCIALACCOUNT_PROVIDERS={"openid_connect": { "SERVERS": [{ "id": "cloudron", "name": "${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}", "server_url": "${CLOUDRON_OIDC_ISSUER}", "APP": { "client_id": "${CLOUDRON_OIDC_CLIENT_ID}", "secret": "${CLOUDRON_OIDC_CLIENT_SECRET}" } }] }}

GUNICORN_MEDIA=0
EOF

cat /app/data/env >> /run/tandoor/env

source ${VENV_PATH}/bin/activate

echo "==> Migrating database"
python /app/code/tandoor/manage.py migrate

if [[ ! -d  /run/tandoor/static ]]; then
    echo "==> Generating static files"
    cp -R /app/pkg/static  /run/tandoor/
    python /app/code/tandoor/manage.py collectstatic_js_reverse
    python /app/code/tandoor/manage.py collectstatic --noinput
fi

echo "=> Changing permissions"
chown -R cloudron:cloudron /app/data /run

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Tandoor
